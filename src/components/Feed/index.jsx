import { DataGrid } from "@material-ui/data-grid";
import { Container, Bottom } from "./style";
import { useHistory } from "react-router-dom";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import { FaUsers } from "react-icons/fa";
import { HiOutlineLogout } from "react-icons/hi";
// import Feedback from "../../components/Feedback";
const Feed = ({ users }) => {
  const history = useHistory();

  const columns = [
    { field: "id", headerName: "ID" },
    { field: "name", headerName: "Nome", width: 200 },
    { field: "user", headerName: "Usuário", width: 200 },
    {
      field: "email",
      headerName: "Email",
      width: 300,
    },
  ];

  const changeRoute = (id) => {
    history.push(`/users/feedbacks/${id}`);
  };

  const setLogout = () => {
    window.localStorage.clear();
    history.push("/");
  };

  const setValue = (value) => {
    value === "users" ? history.push("/users") : setLogout();
  };

  return (
    <>
      <Container>
        <DataGrid
          rows={users}
          columns={columns}
          pageSize={10}
          onRowClick={(e) => {
            changeRoute(e.data.id);
          }}
        />
      </Container>
      <Bottom>
        <BottomNavigation
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        >
          <BottomNavigationAction
            label="Recents"
            value="users"
            icon={<FaUsers />}
          />
          <BottomNavigationAction
            label="Recents"
            value="logout"
            icon={<HiOutlineLogout />}
          />
        </BottomNavigation>
      </Bottom>
    </>
  );
};

export default Feed;
