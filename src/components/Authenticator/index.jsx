import { useEffect, useState } from "react";
import { Route, Switch, useHistory } from "react-router-dom";

import Home from "../../components/Home";
import Feed from "../Feed";
import Feedback from "../../components/Feedback";
import NewFeedback from "../NewFeedback";

import { CircularProgress } from "@material-ui/core";
import api from "../../api";
const Authentication = () => {
  const [isAuthenticated, setAuthentication] = useState(undefined);
  const [users, setUsers] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const token = window.localStorage.getItem("authToken");

    if (!token) {
      setAuthentication(false);
    }

    api
      .get("http://ka-users-api.herokuapp.com/users")
      .then((res) => {
        setAuthentication(true);
        setUsers(res.data);
      })
      .catch(() => {
        setAuthentication(false);
      });
  }, [history, setAuthentication]);

  if (isAuthenticated === undefined) {
    return <CircularProgress color="secondary" />;
  }

  if (isAuthenticated === false) {
    return (
      <Switch>
        <Route path="/">
          <Home setAuthentication={setAuthentication} />
        </Route>
      </Switch>
    );
  }

  if (isAuthenticated) {
    return (
      <Switch>
        <Route exact path="/">
          <Home setAuthentication={setAuthentication} />
        </Route>
        <Route exact path="/users">
          {users.length > 0 ? <Feed users={users} /> : "Carregando"}
        </Route>
        <Route exact path="/users/feedbacks/:id">
          <Feedback />
        </Route>
        <Route path="/users/feedbacks/:id/new">
          <NewFeedback />
        </Route>
      </Switch>
    );
  }
};

export default Authentication;
