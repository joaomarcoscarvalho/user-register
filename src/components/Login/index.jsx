import { useState } from "react";
import { useForm } from "react-hook-form";
import { Button, TextField } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import api from "../../api";
const Login = ({ setAuthentication }) => {
  const { register, handleSubmit } = useForm({});
  const history = useHistory();

  const [error, setError] = useState(false);

  const handleData = (data) => {
    api
      .post("https://ka-users-api.herokuapp.com/authenticate", { ...data })
      .then((res) => {
        window.localStorage.setItem("authToken", res.data.auth_token);
        setAuthentication(true);
        history.push("/users");
      })
      .catch(() => setError(true));
  };

  return (
    <form onSubmit={handleSubmit(handleData)}>
      <TextField
        id="standard-basic"
        type="text"
        label="Usuário"
        name="user"
        inputRef={register}
      />
      <TextField
        inputRef={register}
        id="standard-basic"
        type="password"
        label="Senha"
        name="password"
        className="input"
      />
      {error && (
        <p style={{ color: "red", fontWeight: "bold", fontSize: "0.8rem" }}>
          Dados incorretos
        </p>
      )}
      <Button
        type="submit"
        variant="contained"
        color="secondary"
        className="btn"
        size="large"
      >
        Entrar
      </Button>
    </form>
  );
};

export default Login;
