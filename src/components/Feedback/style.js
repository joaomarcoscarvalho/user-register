import styled from "styled-components";

export const Container = styled.div`
  width: 100vw;
  height: 92vh;
  position: fixed;
  top: 0;

  .MuiDataGrid-root {
    background-color: white;
  }

  /* div {
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
  } */
`;

export const Bottom = styled.div`
  width: 100vw;
  position: fixed;
  bottom: 0;
`;
