import axios from "axios";

const api = axios.create({
  baseURL: "https://ka-users-api.herokuapp.com",
  headers: {
    Authorization: localStorage.getItem("authToken"),
  },
});
export default api;
