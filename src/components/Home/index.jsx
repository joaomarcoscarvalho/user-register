import { Container } from "./styles";
import Form from "../../components/Form";
const Home = ({ setAuthentication }) => {
  return (
    <Container>
      <Form setAuthentication={setAuthentication} />
    </Container>
  );
};

export default Home;
