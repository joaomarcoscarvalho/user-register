import styled from "styled-components";

export const Container = styled.div`
  width: 100vw;
  height: 92vh;
  position: fixed;
  top: 0;

  .MuiDataGrid-root {
    background-color: white;
    box-shadow: 9px 9px 9px -9px #080808;
    text-align: center;
  }
`;

export const Bottom = styled.div`
  width: 100vw;
  position: fixed;
  bottom: 0;
`;
