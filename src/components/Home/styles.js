import styled from "styled-components";

export const Container = styled.div`
  box-sizing: border-box;
  position: relative;
  width: 30%;
  height: 50%;
  display: flex;
  justify-content: center;
  align-items: stretch;

  svg {
    font-size: 10rem;
    margin-bottom: 2rem;
  }
`;
