import { Container } from "../Form/styles";
import { Button } from "@material-ui/core";
import Login from "../Login";
import Register from "../Register";
// import { FaUserAstronaut } from "react-icons/fa";
import { useState } from "react";
const Form = ({ setAuthentication }) => {
  const [register, setRegister] = useState(false);

  return (
    <Container>
      {register ? (
        <Register />
      ) : (
        <Login setAuthentication={setAuthentication} />
      )}
      {register ? (
        <Button
          size="small"
          onClick={() => {
            setRegister(!register);
          }}
        >
          Já é cadastrado?
        </Button>
      ) : (
        <Button
          size="small"
          onClick={() => {
            setRegister(!register);
          }}
        >
          Cadastrar Usuário
        </Button>
      )}
    </Container>
  );
};

export default Form;
