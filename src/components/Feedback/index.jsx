import api from "../../api";

import { useEffect, useState } from "react";

import { useParams } from "react-router-dom";

import { DataGrid } from "@material-ui/data-grid";

import { Container, Bottom } from "./style";

import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import { FaUsers } from "react-icons/fa";
import { HiOutlineLogout } from "react-icons/hi";
import { BiMessageSquareAdd } from "react-icons/bi";
import { useHistory } from "react-router-dom";

import { CircularProgress } from "@material-ui/core";

const Feedback = () => {
  const history = useHistory();
  const { id } = useParams();
  const [feedbacks, setFeedbacks] = useState([]);
  useEffect(() => {
    api
      .get(`https://ka-users-api.herokuapp.com/users/${id}/feedbacks`)
      .then((res) => {
        setFeedbacks(res.data);
      });
  }, []);

  const columns = [
    { field: "id", headerName: "ID" },
    { field: "name", headerName: "Nome", width: 200 },
    { field: "comment", headerName: "Comentário", width: 200 },
    {
      field: "grade",
      headerName: "Nota",
      width: 300,
    },
  ];

  const setLogout = () => {
    window.localStorage.clear();
    history.push("/");
  };

  const setNewFeedback = () => {
    history.push(`/users/feedbacks/${id}/new`);
  };

  const setValue = (value) => {
    if (value === "users") {
      return history.push("/users");
    }

    if (value === "logout") {
      return setLogout();
    }

    return setNewFeedback();
  };

  return (
    <>
      <Container>
        {feedbacks.length > 0 ? (
          <DataGrid rows={feedbacks} columns={columns} pageSize={10} />
        ) : (
          <CircularProgress color="secondary" />
        )}
      </Container>
      <Bottom>
        <BottomNavigation
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        >
          <BottomNavigationAction
            label="Usuarios"
            value="users"
            icon={<FaUsers />}
          />
          <BottomNavigationAction
            label="Adicionar Feedback"
            value="add"
            icon={<BiMessageSquareAdd />}
          />
          <BottomNavigationAction
            label="Sair"
            value="logout"
            icon={<HiOutlineLogout />}
          />
        </BottomNavigation>
      </Bottom>
    </>
  );
};

export default Feedback;
