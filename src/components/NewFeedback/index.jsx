import { Button, TextField } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { useParams, useHistory } from "react-router-dom";
import api from "../../api";
import { Container } from "./styles";
const NewFeedback = () => {
  const history = useHistory();
  const { id } = useParams();
  const { register, handleSubmit } = useForm({});

  const makeAFeedback = (data) => {
    const feedback = { feedback: { ...data } };
    api
      .post(`https://ka-users-api.herokuapp.com/users/${id}/feedbacks`, {
        ...feedback,
      })
      .then(() => {
        history.push(`/users/feedbacks/${id}`);
      });
  };
  return (
    <Container>
      <form onSubmit={handleSubmit(makeAFeedback)}>
        <TextField
          id="standard-basic"
          type="text"
          label="Nome"
          name="name"
          inputRef={register}
        />
        <TextField
          inputRef={register}
          id="standard-basic"
          type="text"
          label="Comentário"
          name="comment"
          className="input"
        />
        <TextField
          inputRef={register}
          id="standard-basic"
          type="text"
          label="Nota"
          name="grade"
          className="input"
        />

        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className="btn"
          size="large"
        >
          Enviar
        </Button>
      </form>
    </Container>
  );
};
export default NewFeedback;
