import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 80vw;

  form {
    display: flex;
    flex-direction: column;
  }

  .input {
    margin-top: 5%;
  }

  .btn {
    margin-top: 12%;
  }
`;
