import Authentication from "./components/Authenticator";
import "./App.css";
function App() {
  return (
    <div className="main">
      <Authentication />
    </div>
  );
}

export default App;
