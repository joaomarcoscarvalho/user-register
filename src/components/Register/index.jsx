import { Button, TextField } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import api from "../../api";
import { useState } from "react";
const Register = () => {
  const schema = yup.object().shape({
    user: yup
      .string()
      .min(6, "Minimo 6 caracteres")
      .required("Campo obrigatório"),
    name: yup
      .string()
      .matches(/^[a-zA-Z\s]+$/, "Apenas Letras")
      .required("Campo obrigatório"),
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    password: yup
      .string()
      .min(6, "Minimo 6 caracteres")
      .required("Campo obrigatório"),
    password_confirmation: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas não são iguais"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const [success, setSuccess] = useState(undefined);

  const handleRegister = (data) => {
    const user = { user: { ...data } };
    console.log({ ...user });
    api
      .post("https://ka-users-api.herokuapp.com/users", { ...user })
      .then(() => {
        setSuccess(true);
      })
      .catch(() => setSuccess(false));
  };

  return (
    <form onSubmit={handleSubmit(handleRegister)}>
      <TextField
        id="standard-basic"
        type="text"
        label="Usuario"
        name="user"
        inputRef={register}
        error={!!errors.user}
        helperText={errors.user?.message}
      />
      <TextField
        inputRef={register}
        id="standard-basic"
        type="text"
        label="Nome Completo"
        name="name"
        className="input"
        error={!!errors.name}
        helperText={errors.name?.message}
      />

      <TextField
        inputRef={register}
        id="standard-basic"
        type="text"
        label="Email"
        name="email"
        className="input"
        error={!!errors.email}
        helperText={errors.email?.message}
      />
      <TextField
        inputRef={register}
        id="standard-basic"
        type="password"
        label="Senha"
        name="password"
        className="input"
        error={!!errors.password}
        helperText={errors.password?.message}
      />
      <TextField
        inputRef={register}
        id="standard-basic"
        type="password"
        label="Confirmar Senha"
        name="password_confirmation"
        className="input"
        error={!!errors.passwordConfirm}
        helperText={errors.passwordConfirm?.message}
      />
      {success ? (
        <p style={{ color: "green", fontWeight: "bold", fontSize: "0.8rem" }}>
          Cadastrado com sucesso
        </p>
      ) : (
        <p style={{ color: "red", fontWeight: "bold", fontSize: "0.8rem" }}>
          Usuário já existe
        </p>
      )}
      <Button
        type="submit"
        variant="contained"
        color="secondary"
        className="btn"
        size="large"
      >
        Cadastrar
      </Button>
    </form>
  );
};

export default Register;
